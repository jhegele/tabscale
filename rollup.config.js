import typescript from 'rollup-plugin-typescript2';
import { terser } from 'rollup-plugin-terser'
import pkg from './package.json';

export default {
    input: 'src/tabscale.ts',
    output: [
        {
            file: pkg.main,
            format: 'umd',
            name: 'TabScale'
        },
        {
            file: pkg.main.replace('.js', '.min.js'),
            format: 'umd',
            name: 'TabScale'
        },
        {
            file: pkg.module,
            format: 'es'
        }
    ],
    external: [
        ...Object.keys(pkg.dependencies || {}),
        ...Object.keys(pkg.peerDependencies || {}),
    ],plugins: [
        typescript({
            typescript: require('typescript'),
        }),
        terser({
            include: [/^.+\.min\.js$/, '*es*']
        })
    ]
}